var SupportForm = function () {

    return {
        
        //Contact Form
        initSupportForm: function () {
	        // Validation
	        $("#support-form").validate({
	            // Rules for form validation
	            rules:
	            {
	                name:
	                {
	                    required: true
	                },
	                email:
	                {
	                    required: true,
	                    email: true
	                },
                    phone:
                    {
                        required: true,
                        phoneUS: true
                    },
	                message:
	                {
	                    required: true,
	                    minlength: 10
	                }
	            },
	                                
	            // Messages for form validation
	            messages:
	            {
	                name:
	                {
	                    required: 'Please enter your name'
	                },
	                email:
	                {
	                    required: 'Please enter your email address',
	                    email: 'Please enter a VALID email address'
	                },
                    phone:
                    {
                        required: 'Please enter your phone number'
                    },
	                message:
	                {
	                    required: 'Please enter your message'
	                }
	            },
	                                
	            // Ajax form submition                  
	            submitHandler: function(form)
	            {
	                $.ajax(
	                {
                        type: 'post',
                        url: 'service',
                        dataType: 'json',
                        data: $(form).serialize(),
                        cache: false,
	                    beforeSend: function()
	                    {
                            $('#submit-service').prop('disabled', true);
	                    },
	                    success: function()
	                    {
	                        $("#support-form").addClass('submited');
	                    }
	                });
                    return false;
	            },
	            
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
    
}();