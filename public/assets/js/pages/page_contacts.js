var ContactPage = function () {

    return {
        
    	//Basic Map
        initMap: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
				lat: 37.2403941,
				lng: -113.2826583
			  });
			  
			  var marker = map.addMarker({
				lat: 37.2403941,
				lng: -113.2826583,
	            title: 'Color Country Appliance, INC.'
		       });
			});
        },

        //Panorama Map
        initPanorama: function () {
		    var panorama;
		    $(document).ready(function(){
		      panorama = GMaps.createPanorama({
		        el: '#panorama',
		        lat : 37.2403941,
		        lng : -113.2826583
		      });
		    });
		}        

    };
}();