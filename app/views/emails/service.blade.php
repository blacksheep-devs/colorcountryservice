<h1>Service Request</h1>

<p>
<strong>Name:</strong> {{ $data['name'] }}<br>
<strong>Email:</strong> {{ $data['email'] }}<br>
<strong>Phone:</strong> {{ $data['phone'] }}<br>
<strong>Alt-Phone:</strong> {{ $data['alt_phone'] }}<br>
<strong>Subject:</strong> {{ $data['subject'] }}<br>
<strong>Message:</strong> {{ $data['message'] }}
</p>