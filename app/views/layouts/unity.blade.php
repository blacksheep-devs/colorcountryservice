@section('head')
    @include('includes.head')
@endsection

@section('scripts')
    @include('includes.scripts')
@endsection

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]--><html lang="en">
	<head>
        @yield('head')
	</head>
	<body>
        <div class="wrapper">
            @include('includes.header')
		    @yield('content')
		    @include('includes.footer')
		    @yield('scripts')
        </div>
		<script>
        console.log('this works');
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60944133-1', 'auto');
  ga('send', 'pageview');

</script>
	</body>
</html>