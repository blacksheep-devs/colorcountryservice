<!--=== Header ===-->
    <div class="header">
        <!-- Topbar -->
        <div class="topbar">
            <div class="container">
                <!-- Topbar Navigation -->
                <ul class="loginbar pull-right">
                    <li><a href="tel:+14353190468"><i class="fa fa-phone"></i>&nbsp;&nbsp;(435)319-0468</a> </li>
                    <li class="topbar-devider"></li>
                    <li><a href="mailto:ben@colorcountryappliance.com"><i class="fa fa-envelope"></i>&nbsp;&nbsp;ben@colorcountryappliance.com</a></li>
                </ul>
                <!-- End Topbar Navigation -->
            </div>
        </div>
        <!-- End Topbar -->

        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img id="logo-header" src="assets/img/logo.png" alt="Logo" class="img-responsive">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">
                        <!-- Home -->
                        <li {{{ $nav == 'home' ? 'class=active' : '' }}} >
                            <a href="{{URL::to('/')}}">Home</a>
                        </li>
                        <!-- End Home -->

                        <!-- About -->
                        <li {{{ $nav == 'about' ? 'class=active' : '' }}}>
                            <a href="about">About Us</a>
                        </li>
                        <!-- End About -->

                        <!-- Service -->
                        <li {{{ $nav == 'service' ? 'class=active' : '' }}}>
                            <a href="service">Schedule Service</a>
                        </li>
                        <!-- End Service -->

                        <!-- Contact -->
                        <li {{{ $nav == 'contact' ? 'class=active' : '' }}}>
                            <a href="contact">Contact Us</a>
                        </li>
                        <!-- End Contact -->

                        <!-- Search Block -->
                        {{--<li>--}}
                            {{--<i class="search fa fa-search search-btn"></i>--}}
                            {{--<div class="search-open">--}}
                                {{--<div class="input-group animated fadeInDown">--}}
                                    {{--<input type="text" class="form-control" placeholder="Search">--}}
                                    {{--<span class="input-group-btn">--}}
                                        {{--<button class="btn-u" type="button">Go</button>--}}
                                    {{--</span>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        <!-- End Search Block -->
                    </ul>
                </div><!--/navbar-collapse-->
            </div>
        </div>
        <!-- End Navbar -->
    </div>
    <!--=== End Header ===-->

