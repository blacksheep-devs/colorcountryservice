    <!--=== Slider ===-->
    <div class="slider-inner">
        <div id="da-slider" class="da-slider">
            <div class="da-slide">
                <h2><i>HONEST, FRIENDLY, </i> <br /><i>PROMPT &amp; RELIABLE.</i><br /> <i> QUALITY SERVICE</i> </h2>
                <p><i>Contact us today for all your</i> <br /> <i> appliance repair, heating, air conditioning,</i> <br /> <i>and general handyman services.</i></p>
                <div class="da-img"><img src="assets/plugins/parallax-slider/img/appliances.png" alt=""></div>
            </div>
            <div class="da-slide">
                <h2><i>FURNACES,  </i><br /><i>WATER HEATERS, </i><br /> <i> AND A/C UNITS</i> </h2>
                <p><i>We provide furnace service,</i> <br /> <i> water heater service &amp; installation,</i> <br /> <i>and air conditioning maintenance and repair.</i></p>
                <div class="da-img"><img src="assets/plugins/parallax-slider/img/heating-ac.png" alt=""></div>
            </div>
            <div class="da-slide">
                <h2><i>HANDYMAN</i> <br /> <i>SERVICES</i></h2>
                <p><i>We provide handyman service</i> <br /> <i>including: plumbing, electrical repair,</i> <br /> <i>and kitchen &amp; bath remodeling. </i></p>
                <div class="da-img"><img src="assets/plugins/parallax-slider/img/tools.png" alt="" /></div>
            </div>
            <div class="da-arrows">
                <span class="da-arrows-prev"></span>
                <span class="da-arrows-next"></span>
            </div>
        </div>
    </div><!--/slider-->
    <!--=== End Slider ===-->