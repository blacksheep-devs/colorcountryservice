<!--=== Footer Version 1 ===-->
<div class="footer-v1">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!-- About -->
                <div class="col-md-6 md-margin-bottom-40">
                    <a href="/"><img class="footer-logo" src="assets/img/logo-light.png" alt=""></a>
                    <p>Color Country Appliance has a proud reputation of providing excellent service to homeowners in Southern Utah.</p>
                </div><!--/col-md-3-->
                <!-- End About -->
                <!-- Link List -->
                <div class="col-md-3 md-margin-bottom-40">

                </div><!--/col-md-3-->
                <!-- End Link List -->

                <!-- Address -->
                <div class="col-md-3 map-img md-margin-bottom-40">
                    <div class="headline"><h2>Contact Us</h2></div>
                    <address class="md-margin-bottom-40">
                        640 S. Toquer Blvd.<br />
                        Toquerville, Utah<br />
                        Phone: (435) 319-0468 <br />
                        Email: <a href="mailto:ben@colorcountryappliance.com" class="">ben@colorcountryappliance.com</a>
                    </address>
                </div><!--/col-md-3-->
                <!-- End Address -->
            </div>
        </div>
    </div><!--/footer-->

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>2014 &copy; Color Country Appliance Inc. All Rights Reserved.</p>
                </div>
                <!-- Social Links -->
                <div class="col-md-6">
                    <ul class="social-icons pull-right">
                        <li><a href="https://www.facebook.com/colorcountryappliance" target="_blank" data-original-title="Facebook" class="rounded-x social_facebook"></a></li>
                        <li><a href="javascript:alert('Google Plus page coming soon!');" data-original-title="Goole Plus" class="rounded-x social_googleplus"></a></li>
                        <li><a href="https://www.linkedin.com/company/color-country-appliance" target="_blank" data-original-title="Linkedin" class="rounded-x social_linkedin"></a></li>
                    </ul>
                </div>
                <!-- End Social Links -->
            </div>
        </div>
    </div><!--/copyright-->
</div>
<!--=== End Footer Version 1 ===-->