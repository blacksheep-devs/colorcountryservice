<!-- Layout -->
@extends('layouts.unity')

@section('head')
    @parent
    <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="assets/plugins/gmap/gmap.js"></script>
    <script type="text/javascript" src="assets/js/pages/page_contacts.js"></script>
    <script type="text/javascript" src="assets/js/forms/contact.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.initSliders();
            ContactPage.initMap();
            ContactForm.initContactForm();
        });
    </script>
@stop

@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Contact Us</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Contact Us</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">
        <div class="row margin-bottom-30">
            <div class="col-md-9 mb-margin-bottom-30">
                <h4>We're happy to answer any of your questions or provide you with an estimate. Just send us a message using the form below with any questions you may have.</h4>
                <br>
                <!-- Contacts -->
                {{ Form::open(array('id' => 'contact-form', 'class' => 'sky-form'))}}
                    <header>Contact Us</header>

                    <fieldset>
                        <div class="row">
                            <section class="col col-6">
                                <label class="label">Name</label>
                                <label class="input">
                                    <i class="icon-append fa fa-user"></i>
                                    {{ Form::text('name', null, array('id' => 'name')) }}
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="label">E-mail</label>
                                <label class="input">
                                    <i class="icon-append fa fa-envelope-o"></i>
                                    {{ Form::email('email', null, array('id' => 'email')) }}
                                </label>
                            </section>
                        </div>

                        <section>
                            <label class="label">Subject</label>
                            <label class="input">
                                <i class="icon-append fa fa-tag"></i>
                                {{ Form::text('subject', null, array('id' => 'subject')) }}
                            </label>
                        </section>

                        <section>
                            <label class="label">Message</label>
                            <label class="textarea">
                                <i class="icon-append fa fa-comment"></i>
                                {{ Form::textarea('message', null, array('size' => '30x4', 'id' => 'message')) }}
                            </label>
                        </section>

                        {{--<section>--}}
                            {{--<label class="checkbox">--}}
                                {{--{{ Form::checkbox('copy',true) }}--}}
                                {{--<i></i>Send a copy to my e-mail address--}}
                                {{--</label>--}}
                        {{--</section>--}}
                    </fieldset>

                    <footer>
                        {{ Form::submit('Submit', array('class' => 'button', 'id' => 'submit-contact')) }}
                    </footer>

                    <div class="message">
                        <i class="rounded-x fa fa-check"></i>
                        <p>Your message was successfully sent!</p>
                    </div>
                {{ Form::close() }}
                <!-- Contacts -->

                <div class="margin-bottom-40"></div>
            </div><!--/col-md-9-->

            <div class="col-md-3">
                <!-- Google Map -->
                <div id="map" class="map map-box map-box-space1 margin-bottom-40">
                </div><!---/map-->
                <!-- End Google Map -->

                <!-- Contacts -->
                <div class="headline"><h2>Contacts</h2></div>
                <ul class="list-unstyled who margin-bottom-30">
                    <li><a href="#"><i class="fa fa-home"></i>640 S Toquer Blvd, Toquerville, UT</a></li>
                    <li><a href="mailto:ben@colorcountryappliance.com"><i class="fa fa-envelope"></i>ben@colorcountryappliance.com</a></li>
                    <li><a href="#"><i class="fa fa-phone"></i>(435)319-0468</a></li>
                    <li><a href="http://colorcountryappliance.com" target="_blank"><i class="fa fa-globe"></i>http://colorcountryappliance.com</a></li>
                </ul>
                <!-- End Contacts -->

                <!-- Business Hours -->
                <div class="headline"><h2>Business Hours</h2></div>
                <ul class="list-unstyled margin-bottom-30">
                    <li><strong>Monday-Friday:</strong> 8am to 5pm</li>
                    <li><strong>Saturday:</strong> Closed</li>
                    <li><strong>Sunday:</strong> Closed</li>
                </ul>
                <!-- End Business Hours -->
            </div><!--/col-md-3-->
        </div><!--/row-->

        <!-- Our Clients -->
        <div id="clients-flexslider" class="flexslider home clients">
            <div class="headline"><h2>Proudly Servicing</h2></div>
            <ul class="slides">
                <!-- One -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/admiral_grey.png" alt="">
                            <img src="assets/img/clients/admiral.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- two -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/amana_grey.png" alt="">
                            <img src="assets/img/clients/amana.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- three -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/whirlpool_grey.png" alt="">
                            <img src="assets/img/clients/whirlpool.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- four -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/frigidaire_grey.png" alt="">
                            <img src="assets/img/clients/frigidaire.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- five -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/lg_grey.png" alt="">
                            <img src="assets/img/clients/lg.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- six -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/subzero_grey.png" alt="">
                            <img src="assets/img/clients/subzero.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- seven -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/speedqueen_grey.png" alt="">
                            <img src="assets/img/clients/speedqueen.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- eight -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/bosch_grey.png" alt="">
                            <img src="assets/img/clients/bosch.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- nine -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/jennaire_grey.png" alt="">
                            <img src="assets/img/clients/jennaire.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- ten -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/hotpoint_grey.png" alt="">
                            <img src="assets/img/clients/hotpoint.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- eleven -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/magicchef_grey.png" alt="">
                            <img src="assets/img/clients/magicchef.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- twelve -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/tappan_grey.png" alt="">
                            <img src="assets/img/clients/tappan.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- thirteen -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/whitewestinghouse_grey.png" alt="">
                            <img src="assets/img/clients/whitewestinghouse.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- fourteen -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/maytag_grey.png" alt="">
                            <img src="assets/img/clients/maytag.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li>
                    <!-- fifteen -->
                        <a href="#">
                            <img src="assets/img/clients/kitchenaid_grey.png" alt="">
                            <img src="assets/img/clients/kitchenaid.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- sixteen -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/wolf_grey.png" alt="">
                            <img src="assets/img/clients/wolf.png" class="color-img" alt="">
                        </a>
                    </li>
            </ul>
        </div><!--/flexslider-->
        <!-- End Our Clients -->
    </div><!--/container-->
    <!--=== End Content Part ===-->
@stop