<!-- Layout -->
@extends('layouts.unity')

@section('head')
    @parent
    <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">
    <link rel="stylesheet" href="assets/plugins/parallax-slider/css/parallax-slider.css">
@stop

@section('scripts')
    @parent
    <!-- JS Implementing Plugins -->
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/parallax-slider/js/modernizr.js"></script>
    <script type="text/javascript" src="assets/plugins/parallax-slider/js/jquery.cslider.js"></script>
    <!-- Validation Form -->
    <script type="text/javascript" src="assets/js/pages/index.js"></script>
    <script type="text/javascript" src="assets/js/pages/page_portfolio.js"></script>
    <script type="text/javascript" src="assets/js/forms/support.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.initSliders();
            PortfolioPage.init();
            Index.initParallaxSlider();
            SupportForm.initSupportForm();
        });
    </script>
@stop

@section('content')
    @include('includes.slider')
    <!--=== Purchase Block ===-->
    <div class="purchase">
        <div class="container">
            <div class="row">
                <div class="col-md-9 animated fadeInLeft"><br>
                    <h1>Let us help with all your appliance and home repair needs.</h1>
                </div>
                <div class="col-md-3 btn-buy animated fadeInRight">
                    <a class="btn-u btn-u-lg" data-toggle="modal" data-target="#responsive"><i class="fa fa-clock-o"></i> Schedule Service Now</a>
                </div>
                <div class="modal fade" id="responsive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            {{ Form::open(array('url' => 'service', 'class' => 'sky-form', 'id' => 'support-form')) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="myModalLabel">Schedule Service</h4>
                                </div>
                                <div class="modal-body">
                                    <!-- Contacts -->
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="label">Name</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-user"></i>
                                                    {{ Form::text('name', null, array('id' => 'name')) }}
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label class="label">E-mail</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-envelope-o"></i>
                                                    {{ Form::email('email', null, array('id' => 'email')) }}
                                                </label>
                                            </section>
                                        </div>
                                        <div class="row">
                                            <section class="col col-6">
                                                <label class="label">Phone Number</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-phone"></i>
                                                    {{ Form::text('phone', null, array('id' => 'phone','placeholder' => '(123)123-1234')) }}
                                                </label>
                                            </section>
                                            <section class="col col-6">
                                                <label class="label">Alternate Phone Number</label>
                                                <label class="input">
                                                    <i class="icon-append fa fa-phone"></i>
                                                    {{ Form::text('alt_phone', null, array('id' => 'alt-phone')) }}
                                                </label>
                                            </section>
                                        </div>

                                        <section>
                                            <label class="label">Subject</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-tag"></i>
                                                {{ Form::text('subject', null, array('id' => 'subject')) }}
                                            </label>
                                        </section>

                                        <section>
                                            <label class="label">Message</label>
                                            <label class="textarea">
                                                <i class="icon-append fa fa-comment"></i>
                                                {{ Form::textarea('message', null, array('size' => '30x2', 'id' => 'message')) }}
                                            </label>
                                        </section>

                                    <div class="g-recaptcha" data-sitekey="6LdzDiYUAAAAAFEt_IxdbvuNT6uCODIIZnSIF_IW"></div>

                                        {{--<section>--}}
                                            {{--<label class="checkbox">--}}
                                            {{--{{ Form::checkbox('copy', true) }}--}}
                                            {{--<i></i>Send a copy to my e-mail address</label>--}}
                                        {{--</section>--}}

                                        <div class="message">
                                            <i class="rounded-x fa fa-check"></i>
                                            <p>Your message was successfully sent!</p>
                                        </div>
                                    <!-- Contacts -->
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
                                    {{ Form::submit('Send Request', array('class' => 'btn-u btn-u-primary', 'id' => 'submit-service')) }}
                                </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div><!--/row-->
    <!-- End Purchase Block -->

    <!--=== Content Part ===-->
    <div class="container content">
        <div class="title-box">
            <div class="title-box-text">We backup our <span class="color-green">service</span> with years of experience!</div>
            <p>We proudly service Hurricane, Laverkin, Toquerville, Springdale, Rockville, Virgin, New Harmony, Kanarraville, Leeds, and Apple Valley.<br>We also service the Cedar Mountain area starting May through September.</p>
        </div>

        <div class="margin-bottom-40"></div>

        <!-- Info Blocks -->
        <div class="row">
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-bolt"></i>
                <div class="info-blocks-in">
                    <h3>Household Appliances</h3>
                    <p>We service kitchen & laundry room appliances including: fridges, ranges, dishwashers, microwaves, dryers, and washers.</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-tint"></i>
                <div class="info-blocks-in">
                    <h3>A/C Service</h3>
                    <p>Summers are HOT in Southern Utah! Let us help with the spring time start-up and maintenance of your A/C.</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-fire"></i>
                <div class="info-blocks-in">
                    <h3>Furnace & Water Heaters</h3>
                    <p>Is it getting chilly in your house? That shower not as hot as it should be? Let us make sure your furnace & water heater are ready for the winter.</p>
                </div>
            </div>
        </div>

        <div class="row margin-bottom-60">
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-cogs"></i>
                <div class="info-blocks-in">
                    <h3>Installation</h3>
                    <p>Need some help hooking up that fancy new appliance? We know that installations can be intimidating, whether installing a new or existing appliance, let us help make it easier.</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-truck"></i>
                <div class="info-blocks-in">
                    <h3>RV Appliance Service</h3>
                    <p>Getting ready for a camping trip? Let us service your RV appliances to make sure they are in good working order for that next big outing!</p>
                </div>
            </div>
            <div class="col-sm-4 info-blocks">
                <i class="icon-info-blocks fa fa-wrench"></i>
                <div class="info-blocks-in">
                    <h3>Handyman</h3>
                    <p>Need a handyman? We do plumbing, electrical repair, kitchen, and bathroom remodeling. We can even help with some tiling and hardwood flooring. We are happy to help make your life less stressful.</p>
                </div>
            </div>
        </div>
        <!-- End Info Blocks -->

        <div class="row text-center">
            <div class="col-md-3">
                <img src="/assets/img/boha-2017.png">
            </div>
            <div class="col-md-3">
                <img src="/assets/img/elite.png">
            </div>
            <div class="col-md-3">
                <img src="/assets/img/soap.png">
            </div>
            <div class="col-md-3">
                <img src="/assets/img/toprated.png">
            </div>
        </div>

        <!-- Service Brands -->
        <div id="clients-flexslider" class="flexslider home clients">
            <div class="headline"><h2>Proudly Servicing</h2></div>
            <ul class="slides">
                <!-- One -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/admiral_grey.png" alt="">
                        <img src="assets/img/clients/admiral.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- two -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/amana_grey.png" alt="">
                        <img src="assets/img/clients/amana.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- three -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/whirlpool_grey.png" alt="">
                        <img src="assets/img/clients/whirlpool.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- four -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/frigidaire_grey.png" alt="">
                        <img src="assets/img/clients/frigidaire.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- five -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/lg_grey.png" alt="">
                        <img src="assets/img/clients/lg.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- six -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/subzero_grey.png" alt="">
                        <img src="assets/img/clients/subzero.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- seven -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/speedqueen_grey.png" alt="">
                        <img src="assets/img/clients/speedqueen.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- eight -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/bosch_grey.png" alt="">
                        <img src="assets/img/clients/bosch.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- nine -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/jennaire_grey.png" alt="">
                        <img src="assets/img/clients/jennaire.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- ten -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/hotpoint_grey.png" alt="">
                        <img src="assets/img/clients/hotpoint.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- eleven -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/magicchef_grey.png" alt="">
                        <img src="assets/img/clients/magicchef.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- twelve -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/tappan_grey.png" alt="">
                        <img src="assets/img/clients/tappan.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- thirteen -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/whitewestinghouse_grey.png" alt="">
                        <img src="assets/img/clients/whitewestinghouse.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- fourteen -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/maytag_grey.png" alt="">
                        <img src="assets/img/clients/maytag.png" class="color-img" alt="">
                    </a>
                </li>
                <li>
                <!-- fifteen -->
                    <a href="#">
                        <img src="assets/img/clients/kitchenaid_grey.png" alt="">
                        <img src="assets/img/clients/kitchenaid.png" class="color-img" alt="">
                    </a>
                </li>
                <!-- sixteen -->
                <li>
                    <a href="#">
                        <img src="assets/img/clients/wolf_grey.png" alt="">
                        <img src="assets/img/clients/wolf.png" class="color-img" alt="">
                    </a>
                </li>
            </ul>
        </div><!--/flexslider-->
        <!-- End Service Brands -->
    </div>
    <!--=== End Content Part ==-->

@stop