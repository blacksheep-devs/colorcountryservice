<!-- Layout -->
@extends('layouts.unity')

@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
    	<div class="container">
            <h1 class="pull-left">About Us</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">About Us</li>
            </ul>
        </div><!--/container-->
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <div class="content container">
        <div class="row team margin-bottom-20">
            <div class="col-md-4">
                <div class="thumbnail-style">
                    <img alt="" src="assets/img/main/ben.jpg" class="img-responsive">
                    <h3><a>Ben DeMille</a> <small>Owner</small></h3>
                    <p>Ben is a native to Utah, where he has lived his entire life. Ben went to school and received a degree in appliance repair. He worked with his father doing appliance repair and handyman jobs for several years prior to getting his degree and continues to attend educational seminars today. Ben never stops learning and has always been a hard worker.</p>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="margin-bottom-10"></div>
                <p><span class="dropcap-bg">C</span>olor Country Appliance is locally owned and operated. With over 15 years of experience, we are confident that we can provide quality service at a competitive price.</p>
                <br>
                <p>Color Country Appliance is proud of our reputation of providing excellent service to homeowners in Southern Utah. Our mission is to continue to build long-lasting relationships with our customers through prompt and friendly service and our commitment to 100% satisfaction... guaranteed!</p><br>

                <blockquote class="margin-bottom-30">
                    <p><em>"Ben is one of the most knowledgeable and helpful technicians in the Southern Utah area. He provides professional and friendly service at competitive rates."</em></p>
                    <small><em>- Proud Customer</em></small>
                </blockquote>


                <div class="row list-style-v1">
                    <div class="col-md-12">
                        <ul class="list-unstyled margin-bottom-35">
                            <li><i class="color-green fa fa-check"></i> Over 12 years experience of factory service for all major appliance brands</li>
                            <li><i class="color-green fa fa-check"></i> 10 years heating and a/c service experience</li>
                            <li><i class="color-green fa fa-check"></i> Plumbing and Electrical repair</li>
                            <li><i class="color-green fa fa-check"></i> Kitchen and Bathroom remodels</li>
                            <li><i class="color-green fa fa-check"></i> Many years doing mountain cabin maintenance and repair</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop