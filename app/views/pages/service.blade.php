<!-- Layout -->
@extends('layouts.unity')

@section('head')
    @parent
    <link rel="stylesheet" href="assets/plugins/flexslider/flexslider.css">
@stop

@section('scripts')
    @parent
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/js/forms/support.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.initSliders();
            SupportForm.initSupportForm();
        });
    </script>
@stop

@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Schedule Service</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="/">Home</a></li>
                <li class="active">Schedule Service</li>
            </ul>
        </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->
    <!--=== Content Part ===-->
        <div class="container content">
            <div class="row margin-bottom-30">
                <div class="col-md-9 mb-margin-bottom-30">
                    <p>These days, life can be hectic and demanding, and we rely on our appliances to help speed things up and keep us comfortable. We are here to help make sure your home and appliances work the way they should. <strong class="color-green">Schedule Now</strong> to ensure your appliances keep working as they should.</p><br>
                    <!-- Contacts -->
                    {{ Form::open(array('id' => 'support-form', 'class' => 'sky-form')) }}
                        <header>Schedule Service</header>

                        <fieldset>
                            <!-- Contacts -->
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Name</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        {{ Form::text('name', null, array('id' => 'name')) }}
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">E-mail</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope-o"></i>
                                        {{ Form::email('email', null, array('id' => 'email')) }}
                                    </label>
                                </section>
                            </div>
                            <div class="row">
                                <section class="col col-6">
                                    <label class="label">Phone Number</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-phone"></i>
                                        {{ Form::text('phone', null, array('id' => 'phone')) }}
                                    </label>
                                </section>
                                <section class="col col-6">
                                    <label class="label">Alternate Phone Number</label>
                                    <label class="input">
                                        <i class="icon-append fa fa-phone"></i>
                                        {{ Form::text('alt_phone', null, array('id' => 'alt_phone')) }}
                                    </label>
                                </section>
                            </div>

                            <section>
                                <label class="label">Subject</label>
                                <label class="input">
                                    <i class="icon-append fa fa-tag"></i>
                                    {{ Form::text('subject', null, array('id' => 'subject')) }}
                                </label>
                            </section>

                            <section>
                                <label class="label">Message</label>
                                <label class="textarea">
                                    <i class="icon-append fa fa-comment"></i>
                                    {{ Form::textarea('message', null, array('size' => '30x2', 'id' => 'message')) }}
                                </label>
                            </section>

                            {{--<section>--}}
                                {{--<label class="checkbox">--}}
                                    {{--{{ Form::checkbox('copy', true) }}--}}
                                    {{--<i></i>Send a copy to my e-mail address--}}
                                {{--</label>--}}
                            {{--</section>--}}

                            <div class="message">
                                <i class="rounded-x fa fa-check"></i>
                                <p>Your message was successfully sent!</p>
                            </div>

                            <div class="g-recaptcha" data-sitekey="6LdzDiYUAAAAAFEt_IxdbvuNT6uCODIIZnSIF_IW"></div>
                        <!-- Contacts -->
                        </fieldset>

                        <footer>
                            {{ Form::submit('Send Request', array('class' => 'btn-u btn-u-primary', 'id' => 'submit-service')) }}
                        </footer>

                        <div class="message">
                            <i class="rounded-x fa fa-check"></i>
                            <p>Your message was successfully sent!</p>
                        </div>
                    {{ Form::close() }}
                    <!-- Contacts -->
                </div><!--/col-md-9-->

                <div class="col-md-3">
                    <!-- Contacts -->
                    <div class="headline"><h2>Contacts</h2></div>
                    <ul class="list-unstyled who margin-bottom-30">
                        <li><a href="#"><i class="fa fa-home"></i>640 S Toquer Blvd, Toquerville, UT</a></li>
                        <li><a href="mailto:ben@colorcountryappliance.com"><i class="fa fa-envelope"></i>ben@colorcountryappliance.com</a></li>
                        <li><a href="#"><i class="fa fa-phone"></i>(435) 319-0468</a></li>
                        <li><a href="#"><i class="fa fa-globe"></i>http://colorcountryappliance.com</a></li>
                    </ul>

                    <!-- Business Hours -->
                    <div class="headline"><h2>Business Hours</h2></div>
                    <ul class="list-unstyled margin-bottom-30">
                        <li><strong>Monday-Friday:</strong> 8am to 5pm</li>
                        <li><strong>Saturday:</strong> Closed</li>
                        <li><strong>Sunday:</strong> Closed</li>
                    </ul>

                    <!-- Why we are? -->
                    <div class="headline"><h2>Service Area's</h2></div>
                    <p>We provide service to the following cities:</p>
                    <ul class="list-unstyled">
                        <li><i class="fa fa-check color-green"></i> Hurricane, LaVerkin, Toquerville</li>
                        <li><i class="fa fa-check color-green"></i> Springdale / Rockville</li>
                        <li><i class="fa fa-check color-green"></i> New Harmony / Kanarraville</li>
                        <li><i class="fa fa-check color-green"></i> Apple Valley & Leeds</li>
                        <li><i class="fa fa-check color-green"></i> Cedar Mountain Area (May - Sept)</li>
                    </ul>
                </div><!--/col-md-3-->
            </div><!--/row-->

            <!-- Our Clients -->
            <div id="clients-flexslider" class="flexslider home clients">
                <div class="headline"><h2>Proudly Servicing</h2></div>
                <ul class="slides">
                    <!-- One -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/admiral_grey.png" alt="">
                            <img src="assets/img/clients/admiral.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- two -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/amana_grey.png" alt="">
                            <img src="assets/img/clients/amana.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- three -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/whirlpool_grey.png" alt="">
                            <img src="assets/img/clients/whirlpool.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- four -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/frigidaire_grey.png" alt="">
                            <img src="assets/img/clients/frigidaire.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- five -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/lg_grey.png" alt="">
                            <img src="assets/img/clients/lg.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- six -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/subzero_grey.png" alt="">
                            <img src="assets/img/clients/subzero.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- seven -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/speedqueen_grey.png" alt="">
                            <img src="assets/img/clients/speedqueen.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- eight -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/bosch_grey.png" alt="">
                            <img src="assets/img/clients/bosch.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- nine -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/jennaire_grey.png" alt="">
                            <img src="assets/img/clients/jennaire.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- ten -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/hotpoint_grey.png" alt="">
                            <img src="assets/img/clients/hotpoint.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- eleven -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/magicchef_grey.png" alt="">
                            <img src="assets/img/clients/magicchef.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- twelve -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/tappan_grey.png" alt="">
                            <img src="assets/img/clients/tappan.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- thirteen -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/whitewestinghouse_grey.png" alt="">
                            <img src="assets/img/clients/whitewestinghouse.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- fourteen -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/maytag_grey.png" alt="">
                            <img src="assets/img/clients/maytag.png" class="color-img" alt="">
                        </a>
                    </li>
                    <li>
                    <!-- fifteen -->
                        <a href="#">
                            <img src="assets/img/clients/kitchenaid_grey.png" alt="">
                            <img src="assets/img/clients/kitchenaid.png" class="color-img" alt="">
                        </a>
                    </li>
                    <!-- sixteen -->
                    <li>
                        <a href="#">
                            <img src="assets/img/clients/wolf_grey.png" alt="">
                            <img src="assets/img/clients/wolf.png" class="color-img" alt="">
                        </a>
                    </li>
                </ul>
            </div><!--/flexslider-->
            <!-- End Our Clients -->
        </div><!--/container-->
        <!--=== End Content Part ===-->
@stop