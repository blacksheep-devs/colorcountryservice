<?php

class HomeController extends BaseController {

	public function getIndex()
	{
		return View::make('pages.home', array(
            'nav' => 'home'
        ));
	}

    public function getAbout()
    {
        return View::make('pages.about', array(
            'nav' => 'about'
        ));
    }

    public function getContact()
    {
        return View::make('pages.contact', array(
            'nav' => 'contact'
        ));
    }

    public function postContact()
    {
        Mail::send('emails.contact', array('data' => Input::except('_token')), function($message)
        {
            $message->to('bendemil58@gmail.com', 'Ben DeMille')->subject('Contact Request');
        });

        $response = array(
            'response' => true,
            'message'  => 'Your information was sent!'
        );

        return Response::json($response);
    }

    public function getService()
    {
        return View::make('pages.service', array(
            'nav' => 'service'
        ));
    }

    public function postService()
    {
        $recaptcha = new \ReCaptcha\ReCaptcha('6LdzDiYUAAAAAP5gFTopGw9V2aqJ7sZg16v6-Dfq');
        $resp = $recaptcha->verify(Input::get('g-recaptcha-response'),$_SERVER['REMOTE_ADDR']);
        if($resp->isSuccess())
        {
            Service::create(Input::except('_token'));

            Mail::send('emails.service', array('data' => Input::except('_token')), function($message)
            {
//            $message->to('bendemil58@gmail.com', 'Ben DeMille')->subject('Service Request');
                $message->to('cory@corydemille.com', 'Cory')->subject('Service Request');
            });

            $response = array(
                'response' => true,
                'message'  => 'Service request sent!'
            );

            return Response::json($response);
        } else {
            $response = array(
                'response' => false,
                'message'  => 'Re-captcha Failed.'
            );

            return Response::json($response);
        }
    }

}
